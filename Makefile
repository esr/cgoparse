# This Makefile simulates Go workspace behavior.
#
# You'll need to run "make setup" to pull in Go dependencies before
# running "make".
#
# lextest is an exerciser for the lexical analyzer.

GOPATH=$(shell pwd)
BIN=$(shell pwd)/bin


make:
	cd src/cgoparse; PATH=$${PATH}:$(BIN) make
	go build lextest

clean:
	cd src/cgoparse; make clean

setup:
	GOPATH=$(GOPATH) go get -u -f modernc.org/goyacc
	GOPATH=$(GOPATH) go get -u -f github.com/cznic/golex
	GOPATH=$(GOPATH) go get -u -f gitlab.com/ianbruene/Kommandant

nuke: clean
	rm -f bin/golex bin/goyacc
