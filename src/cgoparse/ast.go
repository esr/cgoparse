// Copyright 2018 Eric S. Raymond. All rights reserved.
// SPDX-License-Identifier: BSD-2-Clause

//go:generate goyacc -o c-grammar.go -p "cgoparse" c-grammar.y
//go:generate golex -o c-lexer.go c-lexer.l

package cgoparse

import (
	"fmt"
	"io"
	"os"
)

// CParseLexer satisfied the yyLexer interface required by goyacc 
type CParseLexer struct {
	Tokenizer
	SourceName string
}

func NewCParseLexer(source *os.File) CParseLexer {
	var lexer CParseLexer
	lexer.SourceName = source.Name()
	getc := func() byte {
		buf := make([]byte, 1)
		n, err := source.Read(buf)
		if err == io.EOF {
			return 0
		} else if err != nil || n < 1 {
			lexer.Error(err.Error())
			return 0
		}
		return buf[0]
	}
	lexer.Getter = getc
	return lexer
}

func (lexer CParseLexer) Lex(tokval *cgoparseSymType) int {
	tokval.token = lexer.GetToken()
	return tokval.token.Type
}

func (cp CParseLexer) Error(msg string) {
	fmt.Fprint(os.Stderr, msg)
}

func CParse(source *os.File) int {
	return cgoparseParse(NewCParseLexer(source))
}
