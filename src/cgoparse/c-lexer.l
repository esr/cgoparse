%{
package cgoparse

// SPDX-License-Identifier: BSD-2-Clause

import (
	"strings"
       )

// This started out as the 2012 version of Jutta Degener's parser, but
// a few changes have been made to conform to the INRIA grammar.  Identifiers
// are no longer passed to a type checker, they're considered tokens of type 
// NAME. The rule "__func__" was dropped. Then the F_CONSTANT and I_CONSTANT
// types were merged into CONSTANT. Some binary operators lost the _OP suffix.
// OR became BARBAR. LE became LEQ. GE became GEQ. NE became NEQ.

type Whitespan struct {
	Text string
	Char int
	Line int
}

type Tokenizer struct {
	Getter func() byte
	prev byte
	countChars int
	countLines int
	yytext strings.Builder
	whitespace []Whitespan
}

func (tokenizer *Tokenizer) getc() byte {
	c := tokenizer.Getter()
	if c != 0 {
		tokenizer.countChars++
		if c == '\n' {
			tokenizer.countLines++
		}
		tokenizer.yytext.WriteByte(c)
	}
	return c
}

func (tokenizer *Tokenizer) stashWhitespace(hd string) {
	var ws Whitespan
	ws.Text = hd + tokenizer.yytext.String()
	n := len(ws.Text)
	ws.Char = tokenizer.countChars - n 
	ws.Line = tokenizer.countLines - strings.Count(ws.Text, "\n")
	if n > 0 {
		ws.Text = ws.Text[:n-1]
	}
	tokenizer.yytext.Reset()
	tokenizer.whitespace = append(tokenizer.whitespace, ws)
}
	
// Comment consumes the tail of a C block comment
func (tokenizer *Tokenizer) consumeComment(hd string) {
	var c byte

	for {
		c = tokenizer.getc()
		if c == 0 {
			break
		}
		if c == byte('*') {
			for {
				c = tokenizer.getc()
				if c == '*' {
					continue
				}

				if c == '/' {
					tokenizer.stashWhitespace(hd)
					return
				}
			}
			if c == 0 {
				break
			}
		}
	}
	panic("unterminated comment");
}

type Token struct {
	Type int
	Text string
	Char int
	Line int
}

// Tokname returns the string name of a token type (for testing purposes).
func (token *Token) Name() string {
	if token.Type < 128 {
		return string([]byte{byte(token.Type & 0xff)})
	}
	// Yuck.  Unhealthy use of magic parser constant
	return cgoparseTokname(token.Type - 57342)
}

func (token *Token) String() string {
	return token.Text		
}
	
func (token *Token) IsEOF() bool {
	return token.Type == 0		
}
	
// We use a return of a zero byte to signal EOF, so sources cannot contain
// actual NUL bytes. C compilers barf on this anyway.

// The following was adapted from the golex documentation:
//
//    %yyc is a "macro" to access the "current" character.
//
//    %yyn is a "macro" to move to the "next" character.
//
//    %yyb is a "macro" to return the beginning-of-line status
//        (a bool typed value).
//        It is used for patterns like `^re`.
//
//    %yyt is a "macro" to return the top/current start condition
//        (an int typed value).
//        It is used when there are patterns with conditions like `<cond>re`.
//        Example: %yyt startCond



	
// GetToken returns an int which may be either a rune value
// or a number above the int32 range that represents a terminal in the
// grammar. As the name implies this is a pure syntactic token and
// has no assocated type information.
func (tokenizer *Tokenizer) GetToken() Token {
	out := func(toktype int) Token {
		var tok Token
		tok.Type = toktype
		tok.Text = tokenizer.yytext.String()
		n := len(tok.Text)
		tok.Char = tokenizer.countChars - n 
		tok.Line = tokenizer.countLines - strings.Count(tok.Text, "\n")
		if n > 0 {
			tok.Text = tok.Text[:n-1]
		}
		tokenizer.yytext.Reset()
		return tok
	}

	c := tokenizer.getc()

%}

%yyc c
%yyn tokenizer.prev = c; c = tokenizer.getc()
%yyb tokenizer.prev == 0 || tokenizer.prev == byte('\n')

O   [0-7]
D   [0-9]
NZ  [1-9]
L   [a-zA-Z_]
A   [a-zA-Z_0-9]
H   [a-fA-F0-9]
HP  (0[xX])
E   ([Ee][+-]?{D}+)
P   ([Pp][+-]?{D}+)
FS  (f|F|l|L)
IS  (((u|U)(l|L|ll|LL)?)|((l|L|ll|LL)(u|U)?))
CP  (u|U|L)
SP  (u8|u|U|L)
ES  (\\(['"\?\\abfnrtv]|[0-7]{1,3}|x[a-fA-F0-9]+))
WS  [ \t\v\n\f]

%%
"/*"                                    tokenizer.consumeComment("/*")
{WS}*"//".*                             /* consume //-comment */

"auto"					return out(AUTO)
"break"					return out(BREAK)
"case"					return out(CASE)
"char"					return out(CHAR)
"const"					return out(CONST)
"continue"				return out(CONTINUE)
"default"				return out(DEFAULT)
"do"					return out(DO)
"double"				return out(DOUBLE)
"else"					return out(ELSE)
"enum"					return out(ENUM)
"extern"				return out(EXTERN)
"float"					return out(FLOAT)
"for"					return out(FOR)
"goto"					return out(GOTO)
"if"					return out(IF)
"inline"				return out(INLINE)
"int"					return out(INT)
"long"					return out(LONG)
"register"				return out(REGISTER)
"restrict"				return out(RESTRICT)
"return"				return out(RETURN)
"short"					return out(SHORT)
"signed"				return out(SIGNED)
"sizeof"				return out(SIZEOF)
"static"				return out(STATIC)
"struct"				return out(STRUCT)
"switch"				return out(SWITCH)
"typedef"				return out(TYPEDEF)
"union"					return out(UNION)
"unsigned"				return out(UNSIGNED)
"void"					return out(VOID)
"volatile"				return out(VOLATILE)
"while"					return out(WHILE)
"_Alignas"                              return out(ALIGNAS)
"_Alignof"                              return out(ALIGNOF)
"_Atomic"                               return out(ATOMIC)
"_Bool"                                 return out(BOOL)
"_Complex"                              return out(COMPLEX)
"_Generic"                              return out(GENERIC)
"_Imaginary"                            return out(IMAGINARY)
"_Noreturn"                             return out(NORETURN)
"_Static_assert"                        return out(STATIC_ASSERT)
"_Thread_local"                         return out(THREAD_LOCAL)

{L}{A}*					return out(NAME)

{HP}{H}+{IS}?				return out(CONSTANT)
{NZ}{D}*{IS}?				return out(CONSTANT)
"0"{O}*{IS}?				return out(CONSTANT)
{CP}?"'"([^'\\\n]|{ES})+"'"		return out(CONSTANT)

{D}+{E}{FS}?				return out(CONSTANT)
{D}*"."{D}+{E}?{FS}?			return out(CONSTANT)
{D}+"."{E}?{FS}?			return out(CONSTANT)
{HP}{H}+{P}{FS}?			return out(CONSTANT)
{HP}{H}*"."{H}+{P}{FS}?			return out(CONSTANT)
{HP}{H}+"."{P}{FS}?			return out(CONSTANT)

({SP}?\"([^"\\\n]|{ES})*\"{WS}*)+	return out(STRING_LITERAL) //"

"..."					return out(ELLIPSIS)
">>="					return out(RIGHT_ASSIGN)
"<<="					return out(LEFT_ASSIGN)
"+="					return out(ADD_ASSIGN)
"-="					return out(SUB_ASSIGN)
"*="					return out(MUL_ASSIGN)
"/="					return out(DIV_ASSIGN)
"%="					return out(MOD_ASSIGN)
"&="					return out(AND_ASSIGN)
"^="					return out(XOR_ASSIGN)
"|="					return out(OR_ASSIGN)
">>"					return out(RIGHT)
"<<"					return out(LEFT)
"++"					return out(INC)
"--"					return out(DEC)
"->"					return out(PTR)
"&&"					return out(AND)
"||"					return out(BARBAR)
"<="					return out(LEQ)
">="					return out(GEQ)
"=="					return out(EQ)
"!="					return out(NEQ)
";"					return out(';')
("{"|"<%")				return out('{')
("}"|"%>")				return out('}')
","					return out(')')
":"					return out(':')
"="					return out('=')
"("					return out('(')
")"					return out(')')
("["|"<:")				return out('[')
("]"|":>")				return out(']')
"."					return out('.')
"&"					return out('&')
"!"					return out('!')
"~"					return out('~')
"-"					return out('-')
"+"					return out('+')
"*"					return out('*')
"/"					return out('/')
"%"					return out('%')
"<"					return out('<')
">"					return out('>')
"^"					return out('^')
"|"					return out('|')
"?"					return out('?')

{WS}+					/* whitespace separates tokens */
.					/* discard bad characters */

\0					return out(0)
%%

	panic("should never be reached")
}


