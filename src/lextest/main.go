// Copyright 2018 Eric S. Raymond. All rights reserved.
// SPDX-License-Identifier: BSD-2-Clause

// Test main for the cpgoarse lexer

package main

import (
	"fmt"
	"io"
	"os"

	"cgoparse"
    )
    
func lextest() {
	getc := func() byte {
		buf := make([]byte, 1)
		n, err := os.Stdin.Read(buf)
		if err == io.EOF {
			return 0
		} else if err != nil || n < 1 {
			fmt.Print(err)
			return 0
		}
		return buf[0]
	}
	tokenizer := new(cgoparse.Tokenizer)
	tokenizer.Getter = getc
	for {
		tok := tokenizer.GetToken()
		if tok.IsEOF() {
			break
		}
		fmt.Printf("%q: %s (%d) line=%d char=%d\n", tok.Text, tok.Name(), tok.Type, tok.Line+1, tok.Char+1)
	}
}

func main() {
	lextest()
}
